import unittest
from dicutils import SortDic


class DicUtilsTests(unittest.TestCase):
  
    def test_sort_by_value(self):
        dic_a = {"a": 3, "b": 1, "c": 2}
        sorted_dic_by_value = SortDic.by_value(dic_a)
        self.assertEqual(sorted_dic_by_value, {"b": 1, "c": 2, "a": 3, })

    def test_sort_by_key(self):
        dic_a = {"b": 1, "a": 3, "c": 2}
        sorted_dic_by_key = SortDic.by_key(dic_a)
        self.assertEqual(sorted_dic_by_key, {"a": 3, "b": 1, "c": 2})


unittest.main()
