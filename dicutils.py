import operator
from collections import OrderedDict


class SortDict:
    """
    Utils class for sort dic operations
    """
    @staticmethod
    def by_value(dico):
        """
        sort dictionnary by value.
        return an OrderedDict
        """
        result = sorted(dico.items(), key=operator.itemgetter(1))
        return OrderedDict(result)

    @staticmethod
    def by_key(dico):
        """
        sort dictionnary by key.
        return an OrderedDict
        """
        result = sorted(dico.items())
        return OrderedDict(result)


class DictFromFunctions(dict):
    """
    Create a dict from a functions chain.
    """
    def __init__(self, original={}, functions=[]):
        res = original.copy()
        for func in functions:
            res = func(res)
        for k, v in res.items():
            self[k] = v


class ObjFromFunctions():
    """
    Create a class instance from a functions chain.
    dict keys will be instance properties
    """
    def __init__(self, original={}, functions=[]):
        res = original.copy()
        for func in functions:
            res = func(res)
        self.__dict__ = {**self.__dict__, **res}
