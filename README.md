# dicutils

    Simple tools for dictionnary sortable operations, like sort dict by value and sort dict by key.

# samples

```python
from dicutils import SortDic


dic_a = {"a": 3, "b": 1, "c": 2}
sorted_dic_by_value = SortDic.by_value(dic_a)
print(sorted_dic_by_value)
# OrderedDict([('b', 1), ('c', 2), ('a', 3)])

dic_b = {"b": 1, "a": 3, "c": 2}
sorted_dic_by_key = SortDic.by_key(dic_b)
print(sorted_dic_by_key)
# OrderedDict([('a', 3), ('b', 1), ('c', 2)])

```
